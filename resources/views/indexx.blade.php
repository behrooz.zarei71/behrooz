<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table border="1" cellpadding="10">
    @foreach($article as $art)
        <tr>
            <td>{{$art->id}}</td>
            <td>{{$art->title}}</td>
            <td>{{$art->body}}</td>
            <td>{{$art->source}}</td>
            <td><a href="article/{{$art->id}}/editee">edite</a> </td>
            <td>
                <form action="article/{{$art->id}}" method="post">
                    {{method_field('delete')}}
                    @csrf
                    <input type="submit" value="delete">
                </form>
            </td>
        </tr>
@endforeach
</table>
</body>
</html>